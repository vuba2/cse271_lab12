/**
 * Basic Recursion Practice.
 * 
 * @author Brandon
 */
public class Recursion {
    
    /**
     * Gets the value of the base to the power of n.
     * 
     * @param base base number
     * @param n number of times to be repeated or the exponent
     * @return base to power of n
     */
    public static int powerN(int base, int n) {
        if (n==0) {
            return 1; 
        }
        return  (base) * powerN(base, (n-1));
        
    }
    
    /**
     * Finds the the amount of total blocks of the triangle. 
     * 
     * @param row number of rows of triangle
     * @return the number of blocks in triangle
     */
    public static int triangle(int row) {
        if (row ==0) {
            return 0;
        }
        return row + triangle(row-1);
        
    }
    
    public static void main(String[] args) {
        System.out.println(powerN(3,0));
        System.out.println(triangle(0));
        System.out.println(triangle(1));
        System.out.println(triangle(2));
        System.out.println(triangle(100));

        
    }

}
